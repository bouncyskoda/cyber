// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS
// CW Coleman 211025

// used big neopixel 
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN 6
#define NUM_LEDS 64
#define BRIGHTNESS 100

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()
int led1,led2,led3, red, green, blue;
int wait = 1000;
int white = 0;
void loop() {
// turn on leds 
  led1 = random(64);
  led2 = led1 % 1;
  led3 =  led1 % 2;
  red = random(0,25);
  green = random(100,255);
  blue = random(0,25);
  //white = random(0,10);
  white = 0;

    strip.setPixelColor(led1, red, green , blue, white);
    strip.setPixelColor(led2, red, green , blue, white);
    strip.setPixelColor(led3, red, green , blue, white);
    strip.show();
   delay(wait);
   //this loop sets all leds to black
   for ( led1 = 0; led1 < 64; led1++){  
    strip.setPixelColor(led1, 255,0,0,0);
    for ( led2 = 0; led2 < 64; led2++) 
    strip.setPixelColor(led2, 0,255,0,0);
     for ( led3 = 0; led3 < 64; led3++)  
    strip.setPixelColor(led3, 0,0,255,0);
   } //end of for loop
    strip.show();


}
