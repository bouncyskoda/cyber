//This works
int redPin = 4;
int greenPin = 3;
int bluePin = 2;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {
  //red
  setColor(255,0,0);
  delay(250);
  //green
  setColor(0,255,0);
  delay(250); 
  //yellow
  setColor(255, 255, 0); 
  delay(250);
  //blue
  setColor(0,0,255);
  delay(250);
  //purple/megenta
  setColor(255,0,255);
  delay(250);
   //rgb white
  setColor(255,255,255);
  delay(250);
   //cyan
  setColor(0,255,255);
  delay(250);
   //light megenta
  setColor(255,50,255);
  delay(250);
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
